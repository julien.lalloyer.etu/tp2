
import Components from './components/Components.js';
import Img from './components/Img.js';
import data from './data.js';

const title = new Components( 'h1', null, ['La', ' ', 'carte'] );
document.querySelector('.pageTitle').innerHTML = title.render();


const img = new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
document.querySelector( '.pageContent' ).innerHTML = img.render();


const c = new Components(
	'article',
	{name:'class', value:'pizzaThumbnail'},
	[
		new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'),
		'Regina'
	]
);
document.querySelector( '.pageContent' ).innerHTML = c.render();

