import Components from './Components.js';

export default class Img extends Components{
	constructor(attribute){
		super('img',{value: attribute, name: "src"}, null);
	}
}