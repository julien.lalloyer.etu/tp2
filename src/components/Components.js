export default class Component {
	tagName = null;
	children = null;
	attribute = null;
	constructor(tagName, attribute , children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}
	render(){
		if (!this.children){
			if(this.attribute.constructor === Array){
				this.renderChildren();
			}		
			return `<${this.tagName} src="${this.attribute.value}" alt="${this.attribute.name}"/>`;
		}else{			
			if(this.children.constructor === Array){
				this.children = this.children.join('');
			}
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		}
	}renderChildren(){
		if(this.attribute.constructor === Array){
			this.attribute.map(i => i.render());
		}
	}
}